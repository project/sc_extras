<?php
use Drupal\Core\Field\FormatterBase;

/**
 * Created by PhpStorm.
 * User: thaohuynh
 * Date: 6/2/16
 * Time: 11:57 PM
 */
namespace Drupal\sc_extras\Field\Formatter;
use Drupal\Core\Field\FormatterBase;

class FormatterTemplateBase extends FormatterBase {

  /**
   * Builds a renderable array for a field value.
   *
   * @param array  $items
   *   The field values to be rendered.
   * @param string $langcode
   *   The language that should be used to render the field.
   *
   * @return array
   *   A renderable array for $items, as an array of child elements keyed by
   *   consecutive numeric indexes starting from 0.
   */
  public function viewElements(array $items, $langcode) {
    $definition = $this->getPluginDefinition();
    /** @var \Drupal\sc_template\Template\TemplateRender $sc_template */
    $sc_template = \Drupal::service('sc_template');
    $filename    = basename(str_replace('\\', '/', get_class($this)));
    if (!empty($definition['template'])) {
      $template = $definition["__FILE__"] . "/templates/" . $definition['template'];
    }
    else {
      $template = $definition["__FILE__"] . "/templates/" . $filename;
    }
    return $sc_template->getRenderArray($template, $items, $definition['id']);
  }
}