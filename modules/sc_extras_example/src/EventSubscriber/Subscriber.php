<?php

/**
 * @file
 * Contain of Subscriber.php
 */
namespace Drupal\sc_extras_example\EventSubscriber;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class Subscriber implements EventSubscriberInterface{

  /** @inheritdoc */
  public static function getSubscribedEvents() {
    return array('sc_extras_example_demo' => 'onDemoEvent');
  }

  public function onDemoEvent($event) {
    drupal_set_message('Subscriber::sc_extras_example_demo');
  }
}
