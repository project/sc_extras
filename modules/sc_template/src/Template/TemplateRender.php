<?php
/**
 * Created by PhpStorm.
 * User: thaohuynh
 * Date: 15/1/16
 * Time: 4:27 PM
 */

namespace Drupal\sc_template\Template;
use Drupal\service_container\Legacy\Drupal7;

class TemplateRender implements TemplateRenderInterface{
  /**
   * The Drupal7 legacy service.
   *
   * @var \Drupal\service_container\Legacy\Drupal7
   */
  protected $drupal7;

  public function __construct(Drupal7 $drupal7) {
    $this->drupal7 = $drupal7;
  }

  /**
   * Fast render template to html markup.
   *
   * @param $template
   * @param $data
   *
   * @return string Rendered data.
   */
  public function render($template, $data = array(), $template_id = NULL) {
    $render_array = $this->getRenderArray($template, $data, $template_id);
    return render($render_array);
  }

  /**
   * Create template array ready for render.
   *
   * @param $template
   * @param $data
   */
  public function getRenderArray($template, $data = array(), $template_id = NULL) {
    return array(
      'zm_template' => array(
        '#theme'       => 'sc_template',
        '#template'    => $template,
        '#data'        => $data,
        '#template_id' => $template_id,
      )
    );
  }
}