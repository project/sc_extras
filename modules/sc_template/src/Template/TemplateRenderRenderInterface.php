<?php
/**
 * Created by PhpStorm.
 * User: thaohuynh
 * Date: 17/1/16
 * Time: 10:06 PM
 */

namespace Drupal\sc_template\Template;


interface TemplateRenderInterface {
  /**
   * Fast render template to html markup.
   *
   * @param $template
   * @param $data
   *
   * @return string Rendered data.
   */
  function render($template, $data = array(), $template_id = NULL);
  function getRenderArray($template, $data = array(), $template_id = NULL);
}