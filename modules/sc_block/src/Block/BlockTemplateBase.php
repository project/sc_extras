<?php
/**
 * Created by PhpStorm.
 * User: thaohuynh
 * Date: 7/2/16
 * Time: 12:18 AM
 */
namespace Drupal\sc_block\Block;

use Drupal\Core\Field\PluginSettingsBase;

class BlockTemplateBase {
  var $pluginDefinition;

  public function __construct($config, $plugin_id, $pluginDefinition) {
    $this->pluginDefinition = $pluginDefinition;
  }

  public function build() {

    $definition = $this->pluginDefinition;
    /** @var \Drupal\sc_template\Template\TemplateRender $sc_template */
    $sc_template = \Drupal::service('sc_template');
    $filename    = basename(str_replace('\\', '/', get_class($this)));
    if (!empty($definition['template'])) {
      $template = $definition["__FILE__"] . "/templates/" . $definition['template'];
    }
    else {
      $template = $definition["__FILE__"] . "/templates/" . $filename;
    }
    return $sc_template->getRenderArray($template, array(), $definition['id']);
  }
}