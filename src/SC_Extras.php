<?php

/**
 * @file
 * Contain of SC_Extras.php
 */
namespace Drupal\sc_extras;

use Drupal\service_container\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class SC_Extras {

  /**
   * @param \Drupal\service_container\DependencyInjection\ContainerInterface $container
   * @param $tag
   * @return array
   */
  private static function findTaggedServiceIds(ContainerInterface $container, $name) {
    $tags = array();
    foreach ($container->getDefinitions() as $id => $definition) {
      if (!empty($definition['tags'])) {
        foreach ($definition['tags'] as $key => $tag) {
          if ($key === $name) {
            $tags[$id] = array();
          }
        }
      }
    }

    return $tags;
  }

  public static function containerReady(ContainerInterface $container) {
    if (!$container->hasDefinition('event_dispatcher')) {
      return;
    }
    /** @var EventDispatcher $event_dispatcher */
    $event_dispatcher = \Drupal::service('event_dispatcher');
    foreach (self::findTaggedServiceIds($container, 'event_subscriber') as $id => $attributes) {
      $subscriber_def = $container->getDefinition($id);
      $class = $subscriber_def['class'];
      $refClass = new \ReflectionClass($class);
      $interface = 'Symfony\Component\EventDispatcher\EventSubscriberInterface';
      if (!$refClass->implementsInterface($interface)) {
        throw new \InvalidArgumentException(sprintf('Service "%s" must implement interface "%s".', $id, $interface));
      }
      $event_dispatcher->addSubscriber(new $class());
    }
  }
}
