<?php
/**
 * Class AnnotatedClassDiscovery
 *
 * Override AnnotatedClassDiscovery to enable search plugin annotated in
 * folder sites/all/container
 */
namespace Drupal\sc_extras\Plugin\Discovery;

use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Reflection\StaticReflectionParser;
use Drupal\Component\Annotation\Reflection\MockFileFinder;

class AnnotatedClassDiscovery extends \Drupal\service_container_annotation_discovery\Plugin\Discovery\AnnotatedClassDiscovery {
  /**
   * Constructs a new instance.
   *
   * @param string[] $plugin_manager_definition
   *   (optional) An array of namespace that may contain plugin implementations.
   *   Defaults to an empty array.
   * @param string $plugin_definition_annotation_name
   *   (optional) The name of the annotation that contains the plugin definition.
   *   Defaults to 'Drupal\Component\Annotation\Plugin'.
   */
  function __construct($plugin_manager_definition, $plugin_definition_annotation_name = 'Drupal\Component\Annotation\Plugin') {
    $namespaces = array();

    foreach (module_list() as $module_name) {
      $directory = DRUPAL_ROOT . '/' . drupal_get_path('module', $module_name) . '/src/' . trim($plugin_manager_definition['directory'], DIRECTORY_SEPARATOR);
      $namespaces['Drupal\\' . $module_name] = array($directory);
    }

    $this->pluginNamespaces               = new \ArrayObject($namespaces);
    $this->pluginDefinitionAnnotationName = isset($plugin_manager_definition['class']) ? $plugin_manager_definition['class'] : $plugin_definition_annotation_name;
    $this->pluginManagerDefinition        = $plugin_manager_definition;
  }

  public function getDefinitions() {
    $definitions = array();

    $reader = $this->getAnnotationReader();

    // Clear the annotation loaders of any previous annotation classes.
    AnnotationRegistry::reset();
    // Register the namespaces of classes that can be used for annotations.
    AnnotationRegistry::registerLoader('class_exists');

    // Search for classes within all PSR-0 namespace locations.
    foreach ($this->getPluginNamespaces() as $namespace => $dirs) {
      foreach ($dirs as $dir) {
        if (file_exists($dir)) {
          $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS)
          );
          foreach ($iterator as $fileinfo) {
            if ($fileinfo->getExtension() == 'php') {
              $sub_path = $iterator->getSubIterator()->getSubPath();
              $sub_path = $sub_path ? str_replace('/', '\\', $sub_path) . '\\' : '';
              $class    = $namespace . '\\' . str_replace('/', '\\', $this->pluginManagerDefinition['directory']) . '\\' . $sub_path . $fileinfo->getBasename('.php');

              // The filename is already known, so there is no need to find the
              // file. However, StaticReflectionParser needs a finder, so use a
              // mock version.

              $finder = MockFileFinder::create($fileinfo->getPathName());
              $parser = new StaticReflectionParser($class, $finder, TRUE);

              if ($annotation = $reader->getClassAnnotation($parser->getReflectionClass(), $this->pluginDefinitionAnnotationName)) {
                $this->prepareAnnotationDefinition($annotation, $class);
                $definitions[$annotation->getId()] = $annotation->get();
              }
            }
          }
        }
      }
    }

    $this->getDefinitionsCustom($definitions, $reader);
    // Don't let annotation loaders pile up.
    AnnotationRegistry::reset();

    return $definitions;
  }

  private function getDefinitionsCustom(&$definitions, $reader) {
    $search_dir = DRUPAL_ROOT . "/sites/all/container/";
    $dirs       = array_filter(glob($search_dir . "*"), 'is_dir');
    foreach ($dirs as $dir_base) {
      $dir = $dir_base . "/" . $this->pluginManagerDefinition['directory'];
      if (file_exists($dir)) {
        $iterator = new \RecursiveIteratorIterator(
          new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS)
        );
        foreach ($iterator as $fileinfo) {
          if ($fileinfo->getExtension() == 'php') {
            $sub_path  = $iterator->getSubIterator()->getSubPath();
            $sub_path  = $sub_path ? str_replace('/', '\\', $sub_path) . '\\' : '';
            $namespace = str_replace($search_dir, "", $dir_base);
            $class     = $namespace . '\\' . str_replace('/', '\\', $this->pluginManagerDefinition['directory']) . '\\' . $sub_path . $fileinfo->getBasename('.php');
            // The filename is already known, so there is no need to find the
            // file. However, StaticReflectionParser needs a finder, so use a
            // mock version.
            $finder = MockFileFinder::create($fileinfo->getPathName());
            $parser = new StaticReflectionParser($class, $finder, TRUE);

            if ($annotation = $reader->getClassAnnotation($parser->getReflectionClass(), $this->pluginDefinitionAnnotationName)) {
              $this->prepareAnnotationDefinition($annotation, $class);
              $definitions[$annotation->getId()]             = $annotation->get();
              $definitions[$annotation->getId()]['__FILE__'] = str_replace(DRUPAL_ROOT . "/", "", $dir);
            }
          }
        }
      }
    }
  }
}
