<?php

/**
 * @file
 * Contain of SCExtrasServiceProvider.php
 */
namespace Drupal\sc_extras\ServiceContainer\ServiceProvider;

use Drupal\service_container\DependencyInjection\ServiceProviderInterface;
class SCExtrasServiceProvider implements ServiceProviderInterface{

  /**
   * Gets a service container definition.
   *
   * @return array
   *   Returns an associative array with the following keys:
   *     - parameters: Simple key-value store of container parameters
   *     - services: Services like defined in services.yml
   *   factory methods, arguments and tags are supported for services.
   *
   * @see core.services.yml in Drupal 8
   */
  public function getContainerDefinition() {
    $services = array();
    $parameters = array();

    return array(
      'parameters' => $parameters,
      'services' => $services,
    );
  }

  /**
   * Allows to alter the container definition.
   *
   * @param array $container_definition
   *   An associative array with the following keys:
   *     - parameters: Simple key-value store of container parameters.
   *     - services: Services like defined in services.yml
   *     - tags: Associative array keyed by tag names with
   *             array('service_name' => $tag_args) as values.
   *
   * @see ServiceProviderInterface::getContainerDefinition()
   */
  public function alterContainerDefinition(&$container_definition) {
    if (isset($container_definition['parameters']['service_container.plugin_manager_types']['annotated'])){
      $container_definition['parameters']['service_container.plugin_manager_types']['annotated'] = "\\Drupal\\sc_extras\\Plugin\\Discovery\\AnnotatedClassDiscovery";
    }
  }
}
